import unittest
from pathlib import Path
from typing import List
from typing import Union
from unittest.mock import Mock
from unittest.mock import call

import pytest

from calc import AmazonDbHistory
from calc import CsvHistory
from calc import JsonHistory
from calc import UnknownKey
from calc import calc
from calc import history_factory
from calc import parse


@pytest.mark.parametrize('expr, tokens', [
    ('2 + 2 =', [2, '+', 2, '=']),
    ('22+2=', [22, '+', 2, '=']),
    ('22@2=', [22, '@', 2, '=']),
    ('2 + 2 = =', [2, '+', 2, '=', '=']),
    ('2+22==', [2, '+', 22, '=', '=']),
    ('2+22==', [2, '+', 22, '=', '=']),
])
def test_parse(expr: str, tokens: List[Union[int, str]]):
    assert list(parse(expr)) == tokens


@pytest.mark.parametrize('expr, answer', [
    ('2 + 2 =', 4),
    ('2 + 2 = =', 6),
    ('2 + 2 = = =', 8),
    ('3 - 2 =', 1),
    ('22+2=', 24),
])
def test_calc(expr: str, answer: int):
    assert calc(expr) == answer


@pytest.mark.parametrize('op', ['@', 'X'])
def test_calc_error(op: str):
    with pytest.raises(UnknownKey) as e:
        calc(f'2 {op} 2 =')
    assert str(e.value) == op


def test_calc_error_try_except():
    try:
        calc('2 @ 2 =')
    except UnknownKey as e:
        assert str(e) == '@'
    else:
        pytest.fail()


def test_history_json(tmpdir: Path):
    history = JsonHistory(tmpdir / 'history.json')

    assert calc('2 + 2 =', history=history) == 4
    assert history.read() == [4]

    assert calc('2 + 3 =', history=history) == 5
    assert history.read() == [4, 5]


def test_history_csv(tmpdir: Path):
    history = CsvHistory(tmpdir / 'history.csv')

    assert calc('2 + 2 =', history=history) == 4
    assert history.read() == [4]

    assert calc('2 + 3 =', history=history) == 5
    assert history.read() == [4, 5]


def test_history_amazon(tmpdir: Path):
    history = Mock(spec=AmazonDbHistory)
    history.read.side_effect = [
        [4],
        [4, 5],
    ]

    assert calc('2 + 2 =', history=history) == 4
    assert history.read() == [4]

    assert calc('2 + 3 =', history=history) == 5
    assert history.read() == [4, 5]

    assert history.add.mock_calls == [
        call(4),
        call(5),
    ]


def test_history_amazon_path(tmpdir: Path):
    with unittest.mock.patch('calc.AmazonDbHistory', autospec=True):
        history = history_factory()
        history.read.side_effect = [
            [4],
            [4, 5],
        ]

        assert calc('2 + 2 =', history=history) == 4
        assert history.read() == [4]

        assert calc('2 + 3 =', history=history) == 5
        assert history.read() == [4, 5]

        assert history.add.mock_calls == [
            call(4),
            call(5),
        ]
