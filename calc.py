import csv
import json
import re
from abc import ABC
from abc import abstractmethod
from pathlib import Path
from typing import Callable
from typing import Dict
from typing import Iterator
from typing import List
from typing import Optional
from typing import Union


class UnknownKey(Exception):
    pass


class History(ABC):

    @abstractmethod
    def read(self) -> List[int]:
        pass

    @abstractmethod
    def add(self, result: int) -> None:
        pass


class JsonHistory(History):
    _path: Path

    def __init__(self, path: Path):
        self._path = path

    def read(self) -> List[int]:
        if self._path.exists():
            with self._path.open() as f:
                return json.load(f)
        else:
            return []

    def add(self, result: int) -> None:
        history = self.read()
        history.append(result)
        with self._path.open('w') as f:
            json.dump(history, f)


class CsvHistory(History):

    def __init__(self, path: Path):
        self._path = path

    def add(self, result: int) -> None:
        with self._path.open('a') as f:
            writer = csv.writer(f)
            writer.writerow([str(result)])

    def read(self) -> List[int]:
        if self._path.exists():
            with self._path.open() as f:
                return [int(row[0]) for row in csv.reader(f) if row]
        else:
            return []


class AmazonDbHistory(History):

    def __init__(self, url):
        self._url = url

    def read(self) -> List[int]:
        raise Exception("Expensive call!")

    def add(self, result: int) -> None:
        raise Exception("Expensive call!")


keys_re = re.compile(r'\d+|\D')


def parse(keys: str) -> Iterator[Union[int, str]]:
    for key in keys_re.findall(keys):
        if key == ' ':
            continue
        if key.isdigit():
            yield int(key)
        else:
            yield key


_operators: Dict[str, Callable] = {}


def operator(op: str) -> Callable:
    def decorator(func: Callable):
        _operators[op] = func
        return func
    return decorator


@operator('+')
def add(a: int, b: int) -> int:
    return a + b


@operator('-')
def subtract(a: int, b: int) -> int:
    return a - b


def history_factory():
    return AmazonDbHistory('asw://user:pass@amazon.com/db')


def calc(keys: str, *, history: History = None) -> int:
    op: Optional[Callable] = None
    stack = [0]
    for key in parse(keys):
        if isinstance(key, int):
            stack.append(key)
        elif key in _operators:
            op = _operators[key]
        elif key == '=':
            b = stack.pop()
            a = stack[-1]
            result = op(a, b)
            if history is not None:
                history.add(result)
            stack.append(result)
        else:
            raise UnknownKey(key)
    return stack[-1]